#! /bin/bash

FILEPATH=$1;
DIR="$(dirname "${FILEPATH}")";
pandoc --extract-media "${DIR}" -f docx -t markdown -o "${FILEPATH%.*}.md" "${FILEPATH}";